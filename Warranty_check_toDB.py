import  sys
import requests
import json
import  pyodbc as db
from openpyxl import  load_workbook
from openpyxl.compat import range
import datetime
from openpyxl.utils import  get_column_interval
from openpyxl.writer.excel import save_virtual_workbook

APIKEY = 'f7ccdf9e-7d5b-4e69-a109-112e81d9bed3'
Headers = {'Content-type': 'application/json'}
data = {'ID':'4SV8G2S'}

def getWarrantyFromDell(serviceTag):
    URL = "https://api.dell.com/support/assetinfo/v4/getassetwarranty/{}?apikey=".format(serviceTag) + APIKEY
    rst = requests.get(URL,headers=Headers)
    print(rst.url)
    if rst.status_code == 200:
        checkServiceTag = rst.json()
        if len(checkServiceTag['AssetWarrantyResponse'])>0:
            print('success request')
            #json have AssetWarrantyResponse =>
            # AssetHeaderData,ProductHeaderData,AssetEntitlementData
            data = rst.json()
            serviceMax = len(data['AssetWarrantyResponse'][0]['AssetEntitlementData'])
        else:
            print('service Tag is invalid')
    else:
        print('not success')

def getWarrantyServiceAll(serviceTag):
    URL = "https://api.dell.com/support/assetinfo/v4/getassetwarranty/{}?apikey=".format(serviceTag) + APIKEY
    rst = requests.get(URL, headers=Headers)
    print(rst.url)
    if rst.status_code == 200:
        checkServiceTag = rst.json()
        if len(checkServiceTag['AssetWarrantyResponse']) > 0:
            print('success request')
            # json have AssetWarrantyResponse =>
            # AssetHeaderData,ProductHeaderData,AssetEntitlementData
            data = rst.json()
            serviceMax = len(data['AssetWarrantyResponse'][0]['AssetEntitlementData'])
            conn = connectDB()
            print('success request')
            data = rst.json()
            serviceMax = len(data['AssetWarrantyResponse'][0]['AssetEntitlementData'])
            for i in range(0, serviceMax):
                startDate = data['AssetWarrantyResponse'][0]['AssetEntitlementData'][i]['StartDate']
                endDate = data['AssetWarrantyResponse'][0]['AssetEntitlementData'][i]['EndDate']
                serviceLevelDescription = data['AssetWarrantyResponse'][0]['AssetEntitlementData'][i][
                    'ServiceLevelDescription']
                insertServiceAsset(conn=conn, serviceTag=serviceTag, startDate=startDate, endDate=endDate,
                                   serviceDescription=serviceLevelDescription)
            conn.close()
        else:
            print('service Tag is invalid')
    else:
        print('not success')
    
def getServiceTagFromExel():
    obj = load_workbook('D:\Code\dellAPI\Service Tag Dell_.xlsx')
    sheet = obj.active
    max_row = sheet.max_row
    print(max_row)
    for i in range(66,max_row):
        if sheet.cell(row=i,column=5).value != None:
            print('row number : ',i)
            if checkConflict(sheet.cell(row=i,column=5).value) == False:
                getWarrantyServiceAll(sheet.cell(row=i,column=5).value)
                print(sheet.cell(row=i,column=5).value)
            else:
                print("check conflict is True")
        print("------------------------")

def connectDB():
    conn = db.connect('DRIVER={SQL Server};SERVER=35.200.129.201;DATABASE=dell_product;UID=sa;PWD=p@ssw0rd')
    cursor = conn.cursor()
    return cursor

def checkConflict(serviceTag):
    conn = connectDB()
    sql = "select servicetage  from product_service where servicetage='{}'".format(serviceTag)
    if conn.execute(sql):
        return True
    else:
        return False


def insertProductHeader(conn,serviceTag,shipdate,startDate,endDate,country_code,machine_description):
    sql = 'insert into product_header(servicetag,shipdate,startDate,endDate,country_code,machine_description) ' \
          'values({},{},{},{},{},{})'.format(serviceTag,shipdate,startDate,endDate,country_code,machine_description)
    print(sql)
    conn.execute('select * from dbo.product_header')

def insertServiceAsset(conn,serviceTag,startDate,endDate,serviceDescription):
    sql = "insert into product_service(servicetage,startDate,endDate,service_description) " \
          "values('{}','{}','{}','{}')".format(serviceTag,datetime.datetime.strptime(startDate,"%Y-%m-%dT%H:%M:%S").
                                         strftime("%Y%m%d %H:%M %p")
                                               ,datetime.datetime.strptime(endDate,"%Y-%m-%dT%H:%M:%S").
                                         strftime("%Y%m%d %H:%M %p")
    ,serviceDescription)
    print(sql)
    conn.execute(sql)
    conn.commit()


if __name__ == '__main__':
    #conn = connectDB()
    getServiceTagFromExel()
    #checkConflict('447M802')
    #getWarrantyFromDell(serviceTag='SPSZ16430AGX')



